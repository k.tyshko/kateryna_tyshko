import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class PasswordCracker implements Runnable {

	private final String hashValue;
	private final HashFunction hashFunction;
	private final Consumer<String> onPasswordFound;
	private final IWordListGenerator wordListGenerator;
	private final AtomicBoolean isStopped;

	public PasswordCracker(String hashValue,
						   HashFunction hashFunction,
						   Consumer<String> onPasswordFound,
						   IWordListGenerator wordListGenerator,
						   AtomicBoolean isStopped) {
		this.hashValue = hashValue;
		this.hashFunction = hashFunction;
		this.onPasswordFound = onPasswordFound;
		this.wordListGenerator = wordListGenerator;
		this.isStopped = isStopped;
	}

	private boolean isPasswordValid(String word) {
		if (Strings.isNullOrEmpty(word))
			return false;

		final HashCode passwordHashValue = hashFunction
			.newHasher()
			.putString(word, Charsets.UTF_8)
			.hash();

		return passwordHashValue.toString().equals(hashValue);
	}

	@Override
	public void run() {
		long totalProcessed = 0;
		while (!isStopped.get()) {
			try {
				String possiblePassword = wordListGenerator.takeNext();
				if (isPasswordValid(possiblePassword)) {
					System.out.println(Thread.currentThread().getName() + " found the password: " + possiblePassword);
					onPasswordFound.accept(possiblePassword);
				}
				else
					System.out.println(Thread.currentThread().getName() + "Password not valid: " + possiblePassword);

				totalProcessed ++;
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		System.out.println(Thread.currentThread().getName() + " Password checker is stopped, totalProcessed=" + totalProcessed );
	}
}
