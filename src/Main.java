import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		HashFunction md5 = Hashing.md5();
		String hashValue = "69c459dd76c6198f72f0c20ddd3c9447";

		AtomicBoolean isPasswordFound = new AtomicBoolean(false);
		Consumer<String> onPasswordFound = password -> {
			isPasswordFound.set(true);
			System.out.println(password);
		};

		WordListGenerator wordListGenerator = new WordListGenerator(97, 122, 1_000, isPasswordFound);

		ExecutorService taskExecutor = Executors.newFixedThreadPool(4);
		taskExecutor.submit(wordListGenerator);
		taskExecutor.submit(new PasswordCracker(hashValue, md5, onPasswordFound, wordListGenerator, isPasswordFound));
		taskExecutor.submit(new PasswordCracker(hashValue, md5, onPasswordFound, wordListGenerator, isPasswordFound));
		taskExecutor.submit(new PasswordCracker(hashValue, md5, onPasswordFound, wordListGenerator, isPasswordFound));

		taskExecutor.awaitTermination(1, TimeUnit.HOURS);
	}
}
