import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class WordListGenerator implements Runnable, IWordListGenerator {

	private final List<Character> chars;
	private final ArrayBlockingQueue<String> wordList;
	private final AtomicBoolean isStopped;

	public WordListGenerator(int asciiStart, int asciiEnd, int maxQuerySize, AtomicBoolean isStopped) {
		this.wordList = new ArrayBlockingQueue<>(maxQuerySize);
		this.chars = new ArrayList<>();
		this.isStopped = isStopped;

		for (int code = asciiStart; code <= asciiEnd; code++) {
			this.chars.add((char) code);
		}
	}

	private void generateValues(String prefix, long length) throws InterruptedException {
		if (length == 0) {
			wordList.put(prefix);
			return;
		}

		for (Character c: chars) {
			if (isStopped.get())
				return;

			String newPrefix = prefix + c;
			generateValues(newPrefix, length - 1);
		}
	}

	@Override
	public void run() {
		try {
			long wordsLength = 1;
			while (!isStopped.get()) {
				generateValues("", wordsLength);
				wordsLength++;
			}

			System.out.println(Thread.currentThread().getName() + " WordListGenerator is stopped");
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String takeNext() throws InterruptedException {
		return wordList.take();
	}
}
