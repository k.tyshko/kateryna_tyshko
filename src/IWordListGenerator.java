public interface IWordListGenerator {
	String takeNext() throws InterruptedException;
}
